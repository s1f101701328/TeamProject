from django.apps import AppConfig


class BulletinBoardConfig(AppConfig):
    name = 'bulletinboard'
