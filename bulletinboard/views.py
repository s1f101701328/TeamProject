from django.shortcuts import render,redirect
from .models import Bulletinboard

# Create your views here.
def index(request):
    bulletinboard = Bulletinboard.objects.all()
    return render(request,
        'bulletinboard/index.html',
        {'bulletinboard': bulletinboard})

def detail(request, bulletinboard_id):
    bulletinboard = Bulletinboard.objects.filter(id=bulletinboard_id).first()
    return render(request,
        'bulletinboard/detail.html',
        {'bulletinboard': bulletinboard}
    )

def create(request):
    if request.method == 'POST':
        bulletinboard = Bulletinboard(
            name=request.POST.get('name'),
            address=request.POST.get('address'),
            business_hour=request.POST.get('business_hour'),
            detail=request.POST.get('detail')
        )
        bulletinboard.save()
        return redirect('/bulletinboard/'+str(bulletinboard.id))

    return render(request,'bulletinboard/create.html')

def update(request, bulletinboard_id):
    bulletinboard = Bulletinboard.objects.filter(id=bulletinboard_id).first()
    if request.method == 'POST':
        bulletinboard.name=request.POST.get('name')
        bulletinboard.address=request.POST.get('address')
        bulletinboard.business_hour=request.POST.get('business_hour')
        bulletinboard.detail=request.POST.get('detail')
        bulletinboard.save()
        return redirect('/bulletinboard/'+str(bulletinboard.id))
    return render(request,
    'bulletinboard/update.html',
    {'bulletinboard': bulletinboard})
