from django.db import models

# Create your models here.
class Bulletinboard(models.Model):
    """掲示板のモデル"""
    name = models.CharField(max_length=256)
    business_hour = models.CharField(max_length=256)
    address = models.TextField()
    detail = models.TextField()

    def __str__(self):
        return self.name
